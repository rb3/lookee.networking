### Lookee Networking Module ###

This is the networking module of the [Lookee project](https://rb3@bitbucket.org/rb3/lookee). It aims to enable communication between two or more remote instances of Lookee in a way that is as fast, flexible, and robust as possible. 

The following description pertains to the latest implementation, which lives in the "release" branch. The "master" branch is considered to be the stable implementation, but may be significantly outdated.

## Overview ##
In the current implementation, the module has **3 abstraction layers**:

1. The highest abstraction layer is composed of the LNetworking singleton class, the LNFile class, and all the subclasses of the LNRemote abstract class. This set of classes are the interface between the developer and the lower layers of the module.

2. The second abstraction layer is comprised of the LNFolder factory class, the LNAnsible class, the LNFileThumb class, and the LNNode class along with its subclasses. This group of classes make adding new functionality to the module much easier by encapsulating all lower-level networking operations in an intuitive and readable package.

3. The last layer is made up of classes that implement the ObjC protocols defined in the 2nd layer. This layer adapts the different networking libraries for use by the upper layers of the module. In the current implementation, the AllJoyn and the MQTT networking libraries are used in this layer.

The current directory structure is as follows:
```
#!objective-c

Networking
| |--Categories  ==> private implementations not meant to be used directly
| |   :
| |--Classes       ==> primary module classes
| |   |--AllJoyn    ==> layer 3 classes for the AllJoyn library
| |   |--MQTT      ==> layer 3 classes for the MQTT library
| |   :
| |--Protocols   ==> ObjC protocol definitions for use by layer
| :   :
|--Patches      ==> Private automatically applied patches to third-party code
|   :
|--Vendor       ==> Third party libraries and build scripts
:   :
```
The over-all logical architecture of this module looks like this:
```
#!objective-c

// Lookee Class Diagram
  	   LNetworking
	       |
 	 LNBusManager
	 ______|_____
	|			 |
 LNBusObject	 |
	|	       LNFileManager
 LNNode		 	 |
	|		   LNFile
 LNRemote	     |
	|		   LNFileThumb
 LNSession
	|
 LNUser
	|
 LRemoteList

// Lookee Network Overview
Device1										  				 Device2

LNBusObject -------------------{ Network }-----------------> AJNProxyBusObject
AJNProxyBusObject <------------{ Network }------------------ LNBusObject

// Lookee Class Descriptions
LNetworking		-> Main singleton; exposes networking APIs and objects
LNBusManager 	-> Handles discovery, session creation/destruction, session joining
LNBusObject 	-> Handles low-level AllJoyn communication; modelled after remote objects and object proxies
LNNode			-> Wrapper for LNBusObject; exposes methods for easy use of AllJoyn functionality in Objective-C
LNRemote 		-> Wrapper for LNNode; its subclasses are used by the rest of the application as interfaces to LNetworking objects
LNSession 		-> Subclass of LNRemote; represents an ongoing Lookee session
LNUser 			-> Subclass of LNRemote; instances represent the current user of Lookee and remote users connected to the current session
LRemoteList 	-> Subclass of LNRemote; stores instances of objects like an array - it is always synced with its remote counterpart
LNFileManager 	-> Represents the collective remote file server; in reality, it is the collection of files on all devices within range
LNFile 			-> Represents a local or remote file; handles file transfers
LNFileThumb 	-> Contains an LNFile instance; it represents a thumbnail for any given file; handles file thumbnail transfers
```