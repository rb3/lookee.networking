//
//  AJNMessage+Patch.m
//  Writeability
//
//  Created by Ryan Blonna on 27/11/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "AJNMessage+Patch.h"

#import <Utilities/NSObject+Utilities.h>

#import <alljoyn/Message.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark AJNMessage+Patch
#pragma mark -
//*********************************************************************************************************************//





@implementation AJNMessage (Patch)

#pragma mark - Loader
//*********************************************************************************************************************//

+ (void)load
{
    [self overrideDeallocWithBlock:^(__unsafe_unretained AJNMessage *self, void(^dealloc)(void)) {
        BOOL shouldDeleteHandleOnDealloc;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
        NSInvocation *invocation = [self invocationForSelector:@selector(shouldDeleteHandleOnDealloc)];
#pragma clang diagnostic pop
        [invocation invoke];
        [invocation getReturnValue:&shouldDeleteHandleOnDealloc];

        if (shouldDeleteHandleOnDealloc) {
            if ([self handle]) {
                delete static_cast<ajn::Message *>(self.handle);
            }

            [self setHandle:nil];
        }
    }];
}

@end
