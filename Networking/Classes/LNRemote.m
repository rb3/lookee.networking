//
//  LNRemote.m
//  Writeability
//
//  Created by Ryan on 6/18/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNRemote_Private.h"

#import <Utilities/LMacros.h>

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNRemote
#pragma mark -
//*********************************************************************************************************************//




@implementation LNRemote $remote(LNNode)

#pragma mark - Static Objects
//*********************************************************************************************************************//

NS_ENUM(NSUInteger, kLRKeyComponent) {
    kLRKeyComponentPeer,
    kLRKeyComponentGroup
};


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithNode:(LNNode *)node
{
    if ((self = [self initWithNode:node ID:node.ID])) {

    }

    return self;
}

- (id)initWithNode:(LNNode *)node ID:(NSString *)ID
{
    DBGParameterAssert((node != nil) && (ID != nil));

    if ((self = [super init])) {
        _node   = node;

        _ID     = [ID copy];

        [self startMonitoringProperties];
    }

    return self;
}

- (void)dealloc
{
    DBGMessage(@"deallocated %@", self);
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[self class]]) {
        return [self.node isEqual:[object node]];
    }

    return NO;
}

- (NSUInteger)hash
{
    return [self.node hash];
}

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, node: %@, id: %@>",
            $classname,
            self,
            self.node,
            self.ID];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)startMonitoringProperties
{
    NSSet *propertyNames = [self.node.class propertyNames];

    if ([propertyNames count]) {
        @weakify();
        [self.node setPropertyDidChangeVerb:^(NSString *propertyName) {
            @strongify();

            if ([propertyNames containsObject:propertyName]) {
                id propertyValue    = [self valueForKey:propertyName];

                id nodeKeyValue     = [self.node valueForKey:propertyName];

                if (![nodeKeyValue isEqual:propertyValue]) {
                    [self setValue:[self.node valueForKey:propertyName] forKey:propertyName];
                }
            }
        }];

        [self observerForKeyPaths:[propertyNames allObjects]](^(LKVONotification *notification) {
            @strongify();

            id propertyName     = [notification keyPath];
            id propertyValue    = [notification newValue];

            id nodeKeyValue     = [self.node valueForKey:propertyName];

            if (![nodeKeyValue isEqual:propertyValue]) {
                [self.node setValue:propertyValue forKey:propertyName];
            }
        });
    }
}

@end
