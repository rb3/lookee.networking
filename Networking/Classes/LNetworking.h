//
//  LNetworking.h
//  Writeability
//
//  Created by Ryan on 5/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNSession.h"
#import "LNFile.h"



@interface LNetworking : NSObject

+ (void)start;

+ (LNSession *)createSessionWithName:(NSString *)name fileID:(NSString *)fileID;

+ (NSArray *)files;
+ (NSArray *)sessions;

+ (LNSession *)session;

@end

static inline NSString *LNetworkingCurrentSessionName() {
    return [[LNetworking session] name];
}

static inline LNSessionType LNetworkingCurrentSessionType() {
    return [[LNetworking session] type];
}

static inline NSArray *LNetworkingCurrentSessionUsers() {
    return [[LNetworking session] users];
}

static inline BOOL LNetworkingIsCurrentDeviceSessionHost() {
    return LNetworkingCurrentSessionType() == kLNSessionTypeHosted;
}


static inline BOOL LNetworkingIsUserLinkedToCurrentSession(LNUser *user) {
    return [[LNetworking session] isLinkedUser:user];
}

static inline void LNetworkingLinkUserToCurrentSession(LNUser *user) {
    [[LNetworking session] linkUser:user];
}

static inline void LNetworkingUnlinkUserFromCurrentSession(LNUser *user) {
    [[LNetworking session] unlinkUser:user];
}


FOUNDATION_EXTERN NSString *const kLNetworkingFoundSessionNotification;
FOUNDATION_EXTERN NSString *const kLNetworkingLostSessionNotification;

FOUNDATION_EXTERN NSString *const kLNetworkingUpdatedFileAnnouncementNotification;
FOUNDATION_EXTERN NSString *const kLNetworkingFinishedFileDownloadNotification;
FOUNDATION_EXTERN NSString *const kLNetworkingAbortedFileDownloadNotification;

FOUNDATION_EXTERN NSString *const kLNetworkingSessionWasHostedNotification;
FOUNDATION_EXTERN NSString *const kLNetworkingSessionWasJoinedNotification;
FOUNDATION_EXTERN NSString *const kLNetworkingSessionWasSelectedNotification;
FOUNDATION_EXTERN NSString *const kLNetworkingSessionWasLeftNotification;