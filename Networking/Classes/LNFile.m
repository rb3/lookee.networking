//
//  LNFile.m
//  Writeability
//
//  Created by Ryan on 5/24/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNFile_Private.h"

#import "LNFileManager.h"

#import <Utilities/LMacros.h>
#import <Utilities/LPath.h>
#import <Utilities/LPDF.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSData+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNFile
#pragma mark -
//*********************************************************************************************************************//





@implementation LNFile

#pragma mark - Static Objects
//*********************************************************************************************************************//

static StringConst(kLNFileCreationDatesKey);


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSOperationQueue *)loadQueue
{
    static dispatch_once_t  predicate   = 0;
    static NSOperationQueue *queue      = nil;

    dispatch_once(&predicate, ^{
        queue = [NSOperationQueue new];
        [queue setMaxConcurrentOperationCount:1];
    });

    return queue;
}

+ (NSDate *)creationDateForFileWithID:(NSString *)ID
{
    id creationDates    = [$userdefaults objectForKey:kLNFileCreationDatesKey];

    if (!creationDates) {
        creationDates = [NSDictionary new];
    }

    NSDate *date = creationDates[ID];

    if (!date) {
        creationDates = [creationDates mutableCopy];

        creationDates[ID]
        =
        date = [NSDate date];

        [$userdefaults setObject:[creationDates copy] forKey:kLNFileCreationDatesKey];
    }

    return date;
}

+ (instancetype)
fileWithManager :(LNFileManager *   )manager
name            :(NSString *        )name
sum             :(NSData *          )sum
size            :(NSUInteger        )size
{
    return [[self alloc] initWithManager:manager name:name sum:sum size:size];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithManager:(LNFileManager *)manager name:(NSString *)name sum:(NSData *)sum size:(NSUInteger)size
{
    DBGParameterAssert((manager != nil) && (name.length > 0) && (sum.length > 0));

    if ((self = [super init])) {
        _manager        = manager;

        _name           = [name copy];
        _ID             = [sum base58Representation];
        _sum            = sum;
        _size           = size;

        _path           = [self.manager.savePath stringByAppendingPathComponent:self.name];
        _dateCreated    = [$this creationDateForFileWithID:self.ID];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[self class]]) {
        return [self.name isEqualToString:[object name]] && [self.sum isEqualToData:[object sum]];
    }

    return NO;
}

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, name: %@, id: %@, created: %@, size: %u, owned: %@, ownerIDs: <%@>>",
            $classname,
            self,
            self.name,
            self.ID,
            self.dateCreated,
            self.size,
            self.isOwned?
            @"YES":@"NO",
            [self.ownerIDs.array componentsJoinedByString:@","]];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)loadWithProgress:(void(^)(float))progress completion:(void(^)(BOOL))completion isPriority:(BOOL)priority
{
    if ([self isOwned]) {
        completion(YES);
        return;
    }

    NSOperationQueue    *queue      = $thisqueue;

    @weakify();

    NSOperation         *operation  = [NSBlockOperation blockOperationWithBlock:^{
        @strongify();

        [queue addOperationWithBlock:^{
            [self.manager downloadFile:self withProgress:progress completion:completion];
        }];
    }];

    if (priority) {
        [operation setQueuePriority:NSOperationQueuePriorityHigh];
    }

    [$this.loadQueue addOperation:operation];
}

- (void)pauseLoad
{
    if (![self isOwned] && [self isTransferring]) {
        [self.manager pauseDownloadOfFile:self];
    }
}

- (void)cancelLoad
{
    if (![self isOwned] && [self isTransferring]) {
        [self.manager cancelDownloadOfFile:self];
    }
}

@end
