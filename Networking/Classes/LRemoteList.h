//
//  LNNodeArray.h
//  Writeability
//
//  Created by Ryan on 6/19/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//

// TODO: Use CoreData as the backing store


#import "LNRemote.h"
#import "LRemoteObject.h"


// TODO: Disallow iteration of children
@interface LRemoteList : LNRemote <NSFastEnumeration>

@property (nonatomic, readonly  , assign) NSUInteger    count;

@property (nonatomic, readwrite , strong) void          (^didInsertAtIndexVerb)(NSUInteger);
@property (nonatomic, readwrite , strong) void          (^willRemoveAtIndexVerb)(NSUInteger);


- (id)objectAtIndex:(NSUInteger)index;
- (id)objectAtIndexedSubscript:(NSUInteger)index;
- (id)lastObject;

- (NSUInteger)indexOfObject:(id<LRemoteObject>)object;

@end

@interface LRemoteArray : LRemoteList

- (void)addObject:(id<LRemoteObject>)object;
- (void)insertObject:(id<LRemoteObject>)object atIndex:(NSUInteger)index;
- (void)setObject:(id<LRemoteObject>)object atIndexedSubscript:(NSUInteger)index;

- (LRemoteArray *)insertArrayAtLastIndex;
- (LRemoteArray *)insertArrayAtIndex:(NSUInteger)index;

- (void)removeLastObject;
- (void)removeObjectAtIndex:(NSUInteger)index;

- (void)removeObject:(id<LRemoteObject>)object;

@end