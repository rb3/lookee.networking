//
//  LParticipant_Private.h
//  Writeability
//
//  Created by Ryan on 5/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNUser.h"

#import "LNRemote_Private.h"




@interface LNUser ()

@property (nonatomic, readwrite, assign, getter = isHost    ) BOOL host;

@property (nonatomic, readwrite, strong) NSString           *name;

@property (nonatomic, readwrite, strong) LRemoteList        *datapoints;


+ (instancetype)userWithSession:(LNRemote *)session ID:(NSString *)ID;

@end

@interface LNSelf ()

+ (instancetype)selfWithSession:(LNRemote *)session;

@end
