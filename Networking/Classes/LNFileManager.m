//
//  LNFileManager.m
//  Writeability
//
//  Created by Ryan on 8/15/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNFileManager.h"

#import "LNConstants.h"

#import "LNFile_Private.h"

#import "LNBusManager.h"

#import "FileTransferModule/FTMFileTransferModule.h"

#import <Utilities/LMacros.h>
#import <Utilities/LPath.h>
#import <Utilities/LTimer.h>
#import <Utilities/LGroupProxy.h>
#import <Utilities/LQueuingProxy.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSString+Utilities.h>
#import <Utilities/NSArray+Utilities.h>
#import <Utilities/NSThread+Block.h>

#import <sys/xattr.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNFileManager
#pragma mark -
//*********************************************************************************************************************//





@interface LNFileManager ()
<
LNBusManagerModule,
LNBusManagerDelegate,
FTMFileAnnouncementReceivedDelegate,
FTMFileAnnouncementSentDelegate,
FTMFileCompletedDelegate
>

@property (nonatomic, readonly  , strong) NSOperationQueue      *queue;

@property (nonatomic, readonly  , strong) LNBusManager          *busManager;

@property (nonatomic, readwrite , strong) FTMFileTransferModule *transfer;

@property (nonatomic, readwrite , strong) NSString              *sessionName;

@property (nonatomic, readonly  , strong) NSMutableOrderedSet   *sessionNames;

@property (nonatomic, readonly  , strong) NSMutableDictionary   *observers;

@property (nonatomic, readonly  , strong) NSMutableDictionary   *completions;

@property (nonatomic, readonly  , strong) LTimer                *updater;

@property (nonatomic, readonly  , strong) LGroupProxy           <LNFileManagerDelegate>*delegates;

@end

@implementation LNFileManager

#pragma mark - Static Objects
//*********************************************************************************************************************//

static NSString *const  kLNFileManagerCacheFileName = @"lookee_filemanager.cache";

static CGFloat  const   kLNFileManagerMonitorUpdateDelay        = 0.1;
static CGFloat  const   kLNFileManagerAnnouncementUpdateDelay   = 1.0;


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSOperationQueue *)fileQueue
{
    static dispatch_once_t  predicate   = 0;
    static NSOperationQueue *queue      = nil;

    dispatch_once(&predicate, ^{
        queue = [NSOperationQueue new];
        [queue setMaxConcurrentOperationCount:1];
    });

    return queue;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)init __ILLEGALMETHOD__;

- (id)initWithDomainName:(NSString *)domainName
{
    if ((self = [super init])) {
        _queue      = [NSOperationQueue new];
        _busManager = [[LNBusManager alloc] initWithDomainName:domainName];

        @weakify();

        _updater    =
        [LTimer
         timerWithTimeInterval:kLNFileManagerAnnouncementUpdateDelay
         block                :^{
             @strongify();

             NSArray *files = [self files];

             [self updateFiles];

             if ([self.delegates.$direct count]) {
                 [self.delegates fileManager:self didUpdateFromOldFiles:files];
             }

             DBGInformation(@"Server updated files: %@", self.files);
         }];

        _sessionName    = [self generateGroupName];

        _sessionNames   = [NSMutableOrderedSet new];

        _observers      = [NSMutableDictionary new];

        _completions    = [LQueuingProxy proxyWithObject:[NSMutableDictionary new]];

        _delegates      = [LGroupProxy proxy];

        [self.queue setMaxConcurrentOperationCount:1];

        [self.sessionNames addObject:self.sessionName];

        [self.busManager addDelegate:self];
        [self.busManager createSessionNamed:self.sessionName];
        [self.busManager registerBusModule:self toSessionNamed:self.sessionName];
    }

    return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize savePath = _savePath;

- (void)setSavePath:(NSString *)savePath
{
    [self stopManagingFilesAtPath:self.savePath];

    _savePath = [savePath copy];

    [self.transfer setDefaultSaveDirectory:savePath];
    [self manageFilesAtPath:savePath];
}

@dynamic domainName;

- (NSString *)domainName
{
    return [self.busManager domainName];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)addDelegate:(id<LNFileManagerDelegate>)delegate
{
    if (![delegate conformsToProtocol:@protocol(LNFileManagerDelegate)]) {
        DBGError(@"%@ does not implement protocol <%@>", delegate, $protostring(LNFileManagerDelegate));
        return;
    }
    
    [self.delegates $addObject:delegate];
}

- (void)removeDelegate:(id<LNFileManagerDelegate>)delegate
{
    [self.delegates $removeObject:delegate];
}

- (void)manageFilesAtPath:(NSString *)path
{
    id observer = self.observers[path];

    if (!observer) {
        u_int8_t value = 1;
        setxattr(path.fileSystemRepresentation, "com.apple.MobileBackup", &value, 1, 0, 0);

        @weakify();

        LTimer *timer
        =
        [LTimer timerWithTimeInterval:kLNFileManagerMonitorUpdateDelay block:^{
            @strongify();

            [self.queue addOperationWithBlock:^{
                [self updateAnnouncedFilesAtPath:path];
            }];
        }];

        self.observers[path] = observer
        =
        [self observerForFileAtPath:path] (^{
            @strongify();

            [$mainqueue addOperationWithBlock:^{
                [timer start];
            }];
        });

        [timer start];
    }
}

- (void)stopManagingFilesAtPath:(NSString *)path
{
    id observer = self.observers[path];

    if (observer) {
        [self destroyFileObserver:observer];
        [self.observers removeObjectForKey:path];
    }
}

- (void)
downloadFile:(LNFile *      )file
withProgress:(void(^)(float))progress
completion  :(void(^)(BOOL) )completion
{
    NSString            *path       = [NSTemporaryDirectory() stringByAppendingPathComponent:file.ID];

    NSOperationQueue    *queue      = $thisqueue;

    NSMutableArray      *ownerIDs   = [NSMutableArray new];

    @weakify();

    for (LNFile *availableFile in [self files]) {
        if ([file.ID isEqualToString:availableFile.ID]) {
            if ([availableFile isOwned]) {
                [$this.fileQueue addOperationWithBlock:^{
                    @strongify();

                    NSError *error = nil;

                    if (![$filemanager copyItemAtPath:availableFile.path toPath:file.path error:&error]) {
                        DBGWarning(@"%@", error.description);
                    }

                    [queue addOperationWithBlock:^{
                        completion(!error);
                    }];
                }];
                return;
            }

            [ownerIDs addObjectsFromArray:[file.ownerIDs array]];
        }
    }

    [ownerIDs addObjectsFromArray:file.ownerIDs.array];

    [self
     downloadFile       :file
     fromOwnerIn        :ownerIDs
     toIntermediatePath :path
     progress           :progress
     completion         :completion];
}

- (void)cancelUploadOfFile:(LNFile *)file
{
    [self.transfer cancelSendingFileWithID:file.sum];
}

- (void)pauseDownloadOfFile:(LNFile *)file
{
    [self.transfer pauseReceivingFileWithID:file.sum];
}

- (void)cancelDownloadOfFile:(LNFile *)file
{
    [self.transfer cancelReceivingFileWithID:file.sum];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (NSString *)generateGroupName
{
    NSCharacterSet  *illegal    = [[NSCharacterSet alphanumericCharacterSet] invertedSet];

    NSString        *name       = [[UIDevice currentDevice] name];
    NSString        *normalized = [[name componentsSeparatedByCharactersInSet:illegal] componentsJoinedByString:@""];

    return [normalized stringByAppendingFormat:@"_%@", self.busManager.peerName.readableRepresentation];
}

- (void)joinServerWithName:(NSString *)name
{
    NSArray *hostedGroups = [self.busManager hostedSessionNames];

    if (![hostedGroups containsObject:self.sessionName]) {
        if ([self.sessionNames containsObject:self.sessionName]) {
            [self.busManager leaveSessionNamed:self.sessionName];
        }
    }

    if (![hostedGroups containsObject:name]) {
        [self.busManager joinSessionNamed:name];
    }

    [self.busManager registerBusModule:self toSessionNamed:name];
    [self setSessionName:name];
}

- (void)updateAnnouncedFilesAtPath:(NSString *)path
{
    NSArray         *descriptors    = [self.transfer announcedLocalFiles];

    NSMutableSet    *announced      = [[NSMutableSet alloc] initWithCapacity:descriptors.count];

    for (FTMFileDescriptor *descriptor in descriptors) {
        [announced addObject:descriptor.filename];
    }

    NSURL           *URL        = [NSURL fileURLWithPath:path];
    NSError         *error      = nil;

    NSArray         *contents   = [[NSFileManager defaultManager]
                                   contentsOfDirectoryAtURL     :URL
                                   includingPropertiesForKeys   :@[NSURLIsDirectoryKey]
                                   options                      :NSDirectoryEnumerationSkipsHiddenFiles
                                   error                        :&error];

    if (error) {
        DBGWarning(@"%@", error.description);
        return;
    }

    NSMutableSet    *available  = [[NSMutableSet alloc] initWithCapacity:contents.count];

    for (NSURL *URL in contents) {
        NSNumber *isDirectory;

        [URL getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error];

        if (!error) {
            if (![isDirectory boolValue]) {
                [available addObject:[URL path]];
            }
        } else {
            DBGWarning(@"%@", error.description); (error = nil);
        }
    }

    NSMutableSet    *removed    = [announced mutableCopy];

    [removed minusSet:available];
    [available minusSet:announced];

    if ([removed count]) {
        [self.transfer stopAnnounceFilePaths:removed.allObjects];
    }

    if ([available count]) {
        [self.transfer announceFilePaths:available.allObjects];
    }
}

- (void)updateFiles
{
    NSMutableArray *files           = [NSMutableArray new];
    NSMutableArray *fileDescriptors = [NSMutableArray new];

    [fileDescriptors addObjectsFromArray:self.transfer.announcedLocalFiles];
    [fileDescriptors addObjectsFromArray:self.transfer.availableRemoteFiles];

    for (LNFile *file in [self files]) {
        @autoreleasepool {
            NSMutableArray      *descriptors    = [NSMutableArray new];
            NSMutableOrderedSet *ownerIDs       = [NSMutableOrderedSet new];

            for (FTMFileDescriptor *descriptor in fileDescriptors) {
                if ([file.name isEqualToString:descriptor.filename] &&
                    [file.sum isEqualToData:descriptor.fileID]) {
                    [ownerIDs addObject:descriptor.owner];
                    continue;
                }

                [descriptors addObject:descriptor];
            }

            if ([ownerIDs count]) {
                BOOL owned = [ownerIDs containsObject:self.busManager.peerName];

                [file setOwnerIDs:ownerIDs];
                [file setOwned:owned];

                [files addObject:file];
            }

            fileDescriptors = descriptors;
        }
    }

    while ([fileDescriptors count]) {
        @autoreleasepool {
            NSMutableArray      *descriptors    = [NSMutableArray new];
            NSMutableOrderedSet *ownerIDs       = [NSMutableOrderedSet new];

            FTMFileDescriptor   *descriptor     = [fileDescriptors lastObject];
            LNFile              *file           = [LNFile
                                                   fileWithManager  :self
                                                   name             :descriptor.filename
                                                   sum              :descriptor.fileID
                                                   size             :descriptor.size];

            [ownerIDs addObject:descriptor.owner];
            [fileDescriptors removeLastObject];

            for (FTMFileDescriptor *descriptor in fileDescriptors) {
                if ([file.name isEqualToString:descriptor.filename] &&
                    [file.sum isEqualToData:descriptor.fileID]) {
                    [ownerIDs addObject:descriptor.owner];
                    continue;
                }

                [descriptors addObject:descriptor];
            }

            BOOL owned = [ownerIDs containsObject:self.busManager.peerName];

            [file setOwnerIDs:ownerIDs];
            [file setOwned:owned];
            [files addObject:file];

            fileDescriptors = descriptors;
        }
    }

    (_files = files);
}

- (void)
downloadFile        :(LNFile *      )file
fromOwnerIn         :(NSArray *     )ownerIDs
toIntermediatePath  :(NSString *    )path
progress            :(void(^)(float))progress
completion          :(void(^)(BOOL) )completion
{
    if ([ownerIDs count]) {
        NSMutableArray *stack = [ownerIDs shuffledMutableCopy];

        if (![self monitorDownloadOfFile:file withProgress:progress]) {
            if ([$filemanager fileExistsAtPath:path]) {
                [self finishDownloadOfFile:file toIntermediatePath:path completion:completion];
                return;
            }

            NSString *fileName  = [path lastPathComponent];
            NSString *directory = [path stringByDeletingLastPathComponent];

            NSString *ownerID;

            while (YES) {
                ownerID = [stack lastObject],[stack removeLastObject];

                if (!ownerID) {
                    [self abortDownloadOfFile:file withCompletion:completion];
                    return;
                }

                FTMStatusCode status
                =
                [[self transfer]
                 requestFileFromPeer:ownerID
                 withFileID         :file.sum
                 andSaveName        :fileName
                 andSaveDirectory   :directory];

                if (status != FTMOK) {
                    DBGWarning(@"Request from %@ failed for %@", ownerID, fileName);
                    continue;
                }

                break;
            }

            DBGInformation(@"Downloading '%@' from '%@'", fileName, ownerID);

            if (progress) {
                [self monitorDownloadOfFile:file withProgress:progress];
            }
        }

        [file setTransferring:YES];

        @weakify();

        self.completions[file.ID] = ^(BOOL success) {
            @strongify();

            if (success) {
                [self finishDownloadOfFile:file toIntermediatePath:path completion:completion];
            } else {
                [self
                 downloadFile       :file
                 fromOwnerIn        :stack.shuffledCopy
                 toIntermediatePath :path
                 progress           :progress
                 completion         :completion];
            }
        };
    } else {
        [self abortDownloadOfFile:file withCompletion:completion];
    }
}

- (BOOL)monitorDownloadOfFile:(LNFile *)file withProgress:(void(^)(float))progress
{
    BOOL loading = NO;

    for (FTMProgressDescriptor *descriptor in [self.transfer receiveProgressList]) {
        if ([file.sum isEqualToData:descriptor.fileID]) {
            loading = YES;

            if (progress) {
                float __block last = (descriptor.bytesTransferred /(float) descriptor.fileSize);

                @weakify();

                [NSThread performBlockInBackground:^{
                    @strongify();

                    BOOL finished;

                    do {
                        @autoreleasepool {
                            finished = YES;

                            for (FTMProgressDescriptor *descriptor in self.transfer.receiveProgressList) {
                                if ([file.sum isEqualToData:descriptor.fileID]) {
                                    finished = NO;

                                    float current = (descriptor.bytesTransferred /(float) descriptor.fileSize);

                                    if (isgreaterequal(fabsf(current - last), .01)) {
                                        last = current;

                                        [NSThread performBlockOnMainThread:^{
                                            progress(current);
                                        }];
                                    }
                                }
                            }
                        }
                    } while (!finished);
                }];
            }
            break;
        }
    }

    return loading;
}

- (void)
finishDownloadOfFile:(LNFile *          )file
toIntermediatePath  :(NSString *        )path
completion          :(void(^)(BOOL)     )completion
{
    NSOperationQueue *queue = $thisqueue;

    @weakify();

    void(^job)(void) = ^{
        @strongify();

        NSError *error = nil;

        if ([$filemanager fileExistsAtPath:file.path]) {
            DBGInformation(@"Removing duplicate %@", file.name);

            if ([$filemanager removeItemAtPath:file.path error:&error]) {
                [$filemanager copyItemAtPath:path toPath:file.path error:&error];
            }
        } else {
            [$filemanager copyItemAtPath:path toPath:file.path error:&error];
        }

        if (error) {
            DBGWarning(@"%@", error.description);
        }

        [queue addOperationWithBlock:^{
            if (!error) {
                [self.completions removeObjectForKey:file.ID];

                [file setTransferring:NO];

                if ([self.delegates.$direct count]) {
                    [self.delegates fileManager:self didDownloadFile:file];
                }

                if (completion) {
                    completion(YES);
                }
            } else {
                [self abortDownloadOfFile:file withCompletion:completion];
            }
        }];
    };
    
    if ([NSOperationQueue currentQueue] != [$this fileQueue]) {
        [$this.fileQueue addOperationWithBlock:job];
    } else {
        job();
    }
}

- (void)abortDownloadOfFile:(LNFile *)file withCompletion:(void(^)(BOOL))completion
{
    [self.completions removeObjectForKey:file.ID];

    [file setTransferring:NO];

    if ([self.delegates.$direct count]) {
        [self.delegates fileManager:self didFailToDownloadFile:file];
    }

    if (completion) {
        completion(NO);
    }
}


#pragma mark - LNBusManagerModule
//*********************************************************************************************************************//

- (void)registerOnBus:(AJNBusAttachment *)bus withSessionIdentifier:(AJNSessionId)identifier
{
    if (![self transfer]) {
        [self setTransfer:[[FTMFileTransferModule alloc] initWithBusAttachment:bus andSessionID:identifier]];

        [self.transfer setFileAnnouncementReceivedDelegate:self];
        [self.transfer setFileAnnouncementSentDelegate:self];
        [self.transfer setFileCompletedDelegate:self];
        [self.transfer setCacheFileWithPath:[LPathCache() stringByAppendingPathComponent:kLNFileManagerCacheFileName]];

        [self setSavePath:LPathDocuments()];
    } else {
        [self.transfer initializeWithBusAttachment:bus andSessionID:identifier];
    }
}


#pragma mark - LNBusManagerDelegate
//*********************************************************************************************************************//

- (void)busManager:(LNBusManager *)manager didAddSessionNamed:(NSString *)sessionName
{
    @weakify();

    [self.queue addOperationWithBlock:^{
        @strongify();

        DBGInformation(@"Found '%@'", sessionName);

        [[self sessionNames]
         insertObject       :sessionName
         atIndex            :
         [[self sessionNames]
          indexOfObject     :sessionName
          inSortedRange     :((NSRange){0, self.sessionNames.count})
          options           :NSBinarySearchingInsertionIndex
          usingComparator   :^(NSString *sessionName, NSString *other) {
              return [sessionName compare:other];
          }]];

        NSString *name = [self.sessionNames lastObject];

        if (![self.sessionName isEqualToString:name]) {
            DBGInformation(@"Connecting to '%@'", name);

            [self joinServerWithName:name];
        }
    }];
}

- (void)busManager:(LNBusManager *)manager didRemoveSessionNamed:(NSString *)sessionName
{
    @weakify();

    [self.queue addOperationWithBlock:^{
        @strongify();

        if ([self.sessionNames containsObject:sessionName]) {
            [self.sessionNames removeObject:sessionName];

            if ([self.sessionName isEqualToString:sessionName]) {
                NSString *name;

                (name = [self.sessionNames lastObject]),
                [self joinServerWithName:name];

                DBGInformation(@"Lost '%@' - connecting to '%@'", sessionName, name);
            }

            [$mainqueue addOperationWithBlock:^{
                [self.updater start];
            }];
        }
    }];
}

- (void)busManager:(LNBusManager *)manager didLoseSessionNamed:(NSString *)sessionName
{
    @weakify();

    [self.queue addOperationWithBlock:^{
        @strongify();

        if (![self.busManager.hostedSessionNames containsObject:self.sessionName]) {
            NSString *name;

            [self.sessionNames removeObject:self.sessionName],
            (name = self.sessionNames.lastObject);

            [self joinServerWithName:name];

            DBGInformation(@"Lost '%@' - connecting to '%@'", self.sessionName, name);

            [$mainqueue addOperationWithBlock:^{
                [self.updater start];
            }];
        }
    }];
}

- (void)
busManager      :(LNBusManager *)manager
didAddPeerNamed :(NSString *    )peerName
toSessionNamed  :(NSString *    )sessionName
{
    @weakify();

    [self.queue addOperationWithBlock:^{
        @strongify();

        if (![self.busManager.peerName isEqualToString:peerName]) {
            [self.transfer requestFileAnnouncementFromPeer:peerName];
        }
    }];
}


#pragma mark - FTMFileAnnouncementReceivedDelegate
//*********************************************************************************************************************//

- (void)receivedAnnouncementForFiles:(NSArray *)fileList andIsFileIDResponse:(BOOL)isFileIDResponse
{
    @weakify();

    [$mainqueue addOperationWithBlock:^{
        @strongify();

        [self.updater start];
    }];
}


#pragma mark - FTMFileAnnouncementSentDelegate
//*********************************************************************************************************************//

- (void)announcementSentWithFailedPaths:(NSArray *)failedPaths
{
    if ([failedPaths count]) {
        DBGWarning(@"Failed to serve paths: %@", failedPaths);
    }

    @weakify();

    [$mainqueue addOperationWithBlock:^{
        @strongify();

        [self.updater start];
    }];
}


#pragma mark - FTMFileCompletedDelegate
//*********************************************************************************************************************//

- (void)fileCompletedForFile:(NSString *)fileName withStatusCode:(int)statusCode
{
    void(^completion)(BOOL) = self.completions[fileName];

    if (completion) {
        completion(statusCode == FTMOK);
    }
}

@end
