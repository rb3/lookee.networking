//
//  ATLinkedObject.h
//  AllJoynTest
//
//  Created by Ryan on 5/21/14.
//  Copyright (c) 2014 lookee. All rights reserved.
//


#import <Utilities/LSerializable.h>



typedef void(^LSendVerbType)(id);
typedef void(^LReceiveVerbType)(void(^)(id));

typedef void(^LMethodReturnVerbType)(NSString *, id);



@class LNBusManager;

@class LNNode;



/* !!!: This class follows the multiton pattern;
 * LNNode instances with the same path are always 
 * exactly the same instances.
 */

@interface LNNode : NSObject

+ (NSString *)interfaceName;

+ (id)findNodeWithPath:(NSString *)path;

+ (id)nodeWithPath:(NSString *)path;


- (id)initWithManager:(LNBusManager *)manager ID:(NSString *)ID;
- (id)initWithParent:(LNNode *)parent ID:(NSString *)ID;


- (BOOL)isLinkedToPeerNamed:(NSString *)peerName inSessionNamed:(NSString *)sessionName;
- (void)linkToPeerNamed:(NSString *)peerName inSessionNamed:(NSString *)sessionName;
- (void)linkToNode:(LNNode *)node;

- (void)unlinkFromPeerNamed:(NSString *)peerName inSessionNamed:(NSString *)sessionName;
- (void)unlink;

- (LSendVerbType)sendMessage:(NSString *)name;
- (LReceiveVerbType)interceptMessage:(NSString *)name;
- (void)ignoreMessage:(NSString *)name;

- (void)fetchPropertyValue:(NSString *)propertyName fromPeerNamed:(NSString *)peerName;
- (void)fetchPropertyValuesFromPeerNamed:(NSString *)peerName;

- (id)firstChild;
- (id)nextChild;

- (void)fetchChildren:(void(^)(NSArray *))block;

@end

@interface LNNode (Properties)

- (NSString *)ID;
- (NSString *)typeID;

- (NSString *)path;

- (LNNode   *)parent;

- (void)setPropertyWillChangeVerb:(void(^)(NSString *))propertyWillChangeVerb;
- (void)setPropertyDidChangeVerb:(void(^)(NSString *))propertyDidChangeVerb;

- (id)remote;
- (id)remote:(LMethodReturnVerbType)verb;

@end

@interface LNNode (Serialization) <LSerializable>

- (id)initWithSimplifiedRepresentation:(id)representation;
- (id)simplifiedRepresentation;

@end
