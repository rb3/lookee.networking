//
//  LNFileThumb.m
//  Writeability
//
//  Created by Ryan on 6/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNFileThumb.h"

#import "LNFile_Private.h"

#import "LNFileManager.h"

#import <Utilities/LMacros.h>
#import <Utilities/LPath.h>

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNFileThumb
#pragma mark -
//*********************************************************************************************************************//




@interface LNFileThumb () <LNFileManagerDelegate>

@property (nonatomic, readwrite , weak) LNFile  *thumbFile;

@property (atomic   , readwrite , copy) void    (^completion)(void);

@end

@implementation LNFileThumb

#pragma mark - Static Objects
//*********************************************************************************************************************//

static NSString *const kLFTFileExtension = @"thumb";


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSCache *)cache
{
    static dispatch_once_t  predicate   = 0;
    static NSCache          *cache      = nil;

    dispatch_once(&predicate, ^{
        cache = [NSCache new];
    });

    return cache;
}

+ (NSOperationQueue *)saveQueue
{
    static dispatch_once_t  predicate   = 0;
    static NSOperationQueue *queue      = nil;

    dispatch_once(&predicate, ^{
        queue = [NSOperationQueue new];
        [queue setMaxConcurrentOperationCount:1];
    });

    return queue;
}

+ (NSOperationQueue *)loadQueue
{
    static dispatch_once_t  predicate   = 0;
    static NSOperationQueue *queue      = nil;

    dispatch_once(&predicate, ^{
        queue = [NSOperationQueue new];
        [queue setMaxConcurrentOperationCount:1];
    });

    return queue;
}

+ (CGSize)size
{
    return ((CGSize){144., 144.});
}

+ (instancetype)thumbWithFile:(LNFile *)file
{
    DBGParameterAssert(file != nil);

    LNFileThumb *instance = [[self alloc] initWithFile:file];
    {
        [instance generateFromFile:file];
    }

    return instance;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithFile:(LNFile *)file
{
    if ((self = [super init])) {
        _file = file;

        [self.file.manager addDelegate:self];
    }

    return self;
}


- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, file: %@>",
            $classname,
            self,
            self.file];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)loadImage:(void(^)(UIImage *))loadBlock
{
    [self loadImage:loadBlock forFile:self.file fromServer:NO];
}

- (void)pauseLoadImage
{
    [self.thumbFile pauseLoad];
}

- (void)cancelLoadImage
{
    [self.thumbFile cancelLoad];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (UIImage *)generateImageFromFileAtPath:(NSString *)path
{
    NSString *fileExtension = [path.pathExtension uppercaseString];

    NSString *selectorName  = [NSString stringWithFormat:@"generateImageFrom%@FileAtPath:", fileExtension];

    SEL selector = NSSelectorFromString(selectorName);

    if ([self respondsToSelector:selector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        return [self performSelector:selector withObject:path];
#pragma clang diagnostic pop
    }

    return nil;
}

- (void)generateFromFile:(LNFile *)file
{
    NSString *thumbName = [file.ID stringByAppendingPathExtension:kLFTFileExtension];
    NSString *thumbPath = [file.path.stringByDeletingLastPathComponent stringByAppendingPathComponent:thumbName];

    if (![$filemanager fileExistsAtPath:thumbPath]) {
        @weakify();

        [$this.saveQueue addOperationWithBlock:^{
            @strongify();

            if (![$filemanager fileExistsAtPath:thumbPath]) {
                UIImage *thumbnail = [$this.cache objectForKey:thumbName];

                if (!thumbnail) {
                    thumbnail = [self generateImageFromFileAtPath:file.path];

                    if (!thumbnail) {
                        DBGWarning(@"Failed to generate %@.thumb", file.ID);
                        return;
                    }

                    [UIImagePNGRepresentation(thumbnail) writeToFile:thumbPath atomically:YES];
                }
            }
        }];
    }
}

- (void)loadImage:(void(^)(UIImage *))loadBlock forFile:(LNFile *)file fromServer:(BOOL)serverLoad
{
    NSString *thumbName = [file.ID stringByAppendingPathExtension:kLFTFileExtension];
    NSString *thumbPath = [file.path.stringByDeletingLastPathComponent stringByAppendingPathComponent:thumbName];

    DBGInformation(@"Attempting to load thumb for '%@' from %@", file.name, serverLoad?@"network":@"memory");

    if (!serverLoad) {
        UIImage *thumbnail = [[$this cache] objectForKey:thumbName];

        if (thumbnail) {
            loadBlock(thumbnail);

            DBGInformation(@"Loaded thumb for '%@'", file.name);
        } else {
            if ([$filemanager fileExistsAtPath:thumbPath]) {
                DBGInformation(@"Found thumb for '%@' on disk", file.name);

                NSOperationQueue *queue = $thisqueue;

                @weakify();

                [$this.loadQueue addOperationWithBlock:^{
                    @strongify();

                    NSData *data = [NSData dataWithContentsOfFile:thumbPath];

                    if (data) {
                        [$this.cache setObject:[UIImage imageWithData:data] forKey:thumbName];

                        [queue addOperationWithBlock:^{
                            loadBlock(thumbnail);

                            DBGInformation(@"Loaded %@", thumbName);
                        }];
                    } else {
                        [queue addOperationWithBlock:^{
                            [self loadImage:loadBlock forFile:file fromServer:YES];
                        }];
                    }
                }];
            } else {
                [self loadImage:loadBlock forFile:file fromServer:YES];
            }
        }
    } else {
        for (LNFile *announced in file.manager.files) {
            if ([announced.name.pathExtension isEqualToString:kLFTFileExtension]) {
                NSString *label = [announced.name stringByDeletingPathExtension];

                if ([label isEqualToString:file.ID]) {
                    [self setThumbFile:announced];
                    break;
                }
            }
        }

        @weakify();

        if ([self thumbFile]) {
            [self.thumbFile loadWithProgress:nil completion:^(BOOL finished) {
                @strongify();

                if (!finished) {
                    DBGWarning(@"Failed to load %@", thumbName);
                    return;
                }
                
                [self loadImage:loadBlock forFile:file fromServer:NO];
            } isPriority:NO];
        } else {
            [self setCompletion:^{
                @strongify();

                [self setCompletion:nil];
                [self loadImage:loadBlock forFile:file fromServer:YES];
            }];
        }
    }
}


#pragma mark - LNFileManagerDelegate
//*********************************************************************************************************************//

- (void)fileManager:(LNFileManager *)manager didUpdateFromOldFiles:(NSArray *)files
{
    void(^completion)(void) = [self completion];

    if (completion) {
        completion();
    }
}

@end
