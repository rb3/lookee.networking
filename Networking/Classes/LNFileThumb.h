//
//  LNFileThumb.h
//  Writeability
//
//  Created by Ryan on 6/4/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>



@class LNFile;



@interface LNFileThumb : NSObject

@property (nonatomic, readonly, weak) LNFile *file;


+ (CGSize)size;

+ (instancetype)thumbWithFile:(LNFile *)file;

- (void)loadImage:(void(^)(UIImage *))loadBlock;
- (void)pauseLoadImage;
- (void)cancelLoadImage;

@end
