//
//  LNNode_Private.h
//  Writeability
//
//  Created by Ryan on 5/23/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNNode.h"



@interface LNNode (ProtectedProperties)

- (LNBusManager *)manager;

@end

@interface LNNode (Virtual)

+ (BOOL)shouldManageChildren;

- (NSString *)encodedValueForKey:(NSString *)key;
- (void)setEncodedValue:(NSString *)value forKey:(NSString *)key;

- (void)receiveSignal:(NSString *)signal fromPeerNamed:(NSString *)peerName;
- (NSString *)invokeMethodString:(NSString *)methodString;

@end
