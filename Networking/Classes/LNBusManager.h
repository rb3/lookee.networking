//
//  LNBusManager.h
//  Writeability
//
//  Created by Ryan on 8/18/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>



@protocol   LNBusManagerDelegate;

@protocol   LNBusManagerModule;

@protocol   LNBusObjectDelegate;

@class      LNBusObject;

@class      AJNBusAttachment;



typedef NS_ENUM(NSUInteger, LNBusManagerConnectivity)
{
    kLNBusManagerConnectivityDisconnected,
    kLNBusManagerConnectivityNone,
    kLNBusManagerConnectivityPoor,
    kLNBusManagerConnectivityModerate,
    kLNBusManagerConnectivityGood
};



@interface LNBusManager : NSObject

@property (nonatomic, readonly, strong) NSArray *discoveredSessionNames;
@property (nonatomic, readonly, strong) NSArray *joinedSessionNames;
@property (nonatomic, readonly, strong) NSArray *hostedSessionNames;

@property (nonatomic, readonly, strong) NSString *domainName;
@property (nonatomic, readonly, strong) NSString *GUID;
@property (nonatomic, readonly, strong) NSString *peerName;


- (id)initWithDomainName:(NSString *)domainName;


- (void)addDelegate:(id<LNBusManagerDelegate>)delegate;
- (void)removeDelegate:(id<LNBusManagerDelegate>)delegate;


- (void)registerBusModule:(id<LNBusManagerModule>)module toSessionNamed:(NSString *)sessionName;

- (void)registerBusObject:(LNBusObject<LNBusObjectDelegate>*)busObject;
- (void)unregisterBusObject:(LNBusObject<LNBusObjectDelegate>*)busObject;


- (id)
remoteObjectWithType:(Class     )type
withPath            :(NSString *)path
atPeerNamed         :(NSString *)peerName
inSessionNamed      :(NSString *)sessionName;

- (NSString *)sessionNameForRemoteObject:(id)object;

- (BOOL)createSessionNamed:(NSString *)sessionName;
- (BOOL)renameSessionNamed:(NSString *)sessionName toName:(NSString *)name;

- (BOOL)joinSessionNamed:(NSString *)sessionName;

- (BOOL)leaveSessionNamed:(NSString *)sessionName;

- (void)setTimeoutInSeconds:(uint32_t)timeout forSessionWithName:(NSString *)sessionName;

- (NSString *)hostNameOfSessionNamed:(NSString *)sessionName;
- (NSArray *)peerNamesInSessionNamed:(NSString *)sessionName;

- (BOOL)isReachablePeerNamed:(NSString *)peerName forTimeout:(uint32_t)timeout;

- (void)getConnectivityToPeerNamed:(NSString *)peerName withBlock:(void(^)(LNBusManagerConnectivity))block;
- (void)startMonitoringConnectivityToPeerNamed:(NSString *)peerName withBlock:(void(^)(LNBusManagerConnectivity))block;
- (void)stopMonitoringConnectivityToPeerNamed:(NSString *)peerName;

- (BOOL)removePeerNamed:(NSString *)peerName fromSessionNamed:(NSString *)sessionName;

- (void)banishPeerNamed:(NSString *)peerName fromSessionNamed:(NSString *)sessionName;
- (void)permitPeerNamed:(NSString *)peerName toSessionNamed:(NSString *)sessionName;

@end


@protocol LNBusManagerModule <NSObject>
@required

- (void)registerOnBus:(AJNBusAttachment *)busAttachment withSessionIdentifier:(uint32_t)identifier;

@end

@protocol LNBusManagerDelegate <NSObject>
@optional

- (void)busManager:(LNBusManager *)manager didAddSessionNamed:(NSString *)sessionName;
- (void)busManager:(LNBusManager *)manager didRemoveSessionNamed:(NSString *)sessionName;

- (void)busManager:(LNBusManager *)manager didLoseSessionNamed:(NSString *)sessionName;

- (void)busManager:(LNBusManager *)manager didAddPeerNamed:(NSString *)peerName toSessionNamed:(NSString *)sessionName;
- (void)busManager:(LNBusManager *)manager didRemovePeerNamed:(NSString *)peerName fromSessionNamed:(NSString *)sessionName;

@end

@protocol LNBusObjectDelegate <NSObject>
@optional

- (void)registerInterfacesWithBus:(AJNBusAttachment *)bus;

@end
