//
//  LRemoteList_Private.h
//  Writeability
//
//  Created by Ryan on 6/23/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LRemoteList.h"

#import "LNRemote_Private.h"



@interface LRemoteList ()

+ (instancetype)listWithNode:(LNNode *)node;

@end

@interface LRemoteArray ()

+ (instancetype)arrayWithAncestor:(LNRemote *)ancestor;

@end
