//
//  LNSession.h
//  Writeability
//
//  Created by Ryan on 5/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>

#import "LNRemote.h"



@class LNSelf;
@class LNUser;


// TODO: Run LNSession on another thread
typedef NS_ENUM(NSInteger, LNSessionStatus)
{
    kLNSessionStatusUnknown,
    kLNSessionStatusJoining,
    kLNSessionStatusActive,
    kLNSessionStatusInactive,
    kLNSessionStatusDisconnected
};

typedef NS_ENUM(NSInteger, LNSessionType)
{
    kLNSessionTypeUnknown,
    kLNSessionTypeHosted,
    kLNSessionTypeRemote,
    kLNSessionTypeInvalid
};



@interface LNSession : LNRemote

@property (nonatomic, readonly  , strong) NSString          *announcement;
@property (nonatomic, readonly  , strong) NSString          *name;

@property (nonatomic, readonly  , assign) LNSessionType     type;
@property (nonatomic, readonly  , assign) LNSessionStatus   status;

@property (nonatomic, readonly  , strong) LNSelf            *me;
@property (nonatomic, readonly  , strong) LNUser            *host;

@property (nonatomic, readonly  , strong) NSArray           *users;

@property (nonatomic, readwrite , copy  ) NSString          *password;


- (void)hostWithPassword:(NSString *)password;
- (void)joinWithPassword:(NSString *)password;

- (void)select;
- (void)leave;

- (void)generateThumbnail;
- (void)loadThumbnail:(void(^)(UIImage *))loadBlock;
- (void)stopLoadThumbnail;

- (BOOL)isLinkedUser:(LNUser *)user;
- (void)linkUser:(LNUser *)user;
- (void)unlinkUser:(LNUser *)user;

@end



FOUNDATION_EXTERN NSString *const kLNSessionConnectivityChangedNotification;

FOUNDATION_EXTERN NSString *const kLNSessionConnectivityKey;

FOUNDATION_EXTERN NSString *const kLNSessionConnectivityGood;
FOUNDATION_EXTERN NSString *const kLNSessionConnectivityModerate;
FOUNDATION_EXTERN NSString *const kLNSessionConnectivityPoor;
FOUNDATION_EXTERN NSString *const kLNSessionConnectivityNone;
FOUNDATION_EXTERN NSString *const kLNSessionConnectivityDisconnected;

FOUNDATION_EXTERN NSString *const kLNSessionJoinedUserNotification;
FOUNDATION_EXTERN NSString *const kLNSessionLeftUserNotification;

FOUNDATION_EXTERN NSString *const kLNSessionOngoingUserSynchronizeNotification;
FOUNDATION_EXTERN NSString *const kLNSessionFailedUserSynchronizeNotification;
FOUNDATION_EXTERN NSString *const kLNSessionFinishedUserSynchronizeNotification;

FOUNDATION_EXTERN NSString *const kLNSessionSynchronizeProgressKey;
FOUNDATION_EXTERN NSString *const kLNSessionSynchronizeFileKey;
