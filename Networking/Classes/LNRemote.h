//
//  LNRemote.h
//  Writeability
//
//  Created by Ryan on 6/18/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>



@interface LNRemote : NSObject

@property (nonatomic, readonly, strong) NSString *ID;

@end
